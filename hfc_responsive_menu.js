// jQuery(document).ready(function(){

    // $('body > div:nth-child(2)').addClass('sidenav');
    // $('body > div:nth-child(2)').attr('id', 'sideaccord');

    // $('#sideaccord > ul').addClass('accordion');
    // $('#sideaccord > ul').attr('id', 'hfcrespmenu');

    // add parent class and toggle class
    // $('#hfcrespmenu li ul').parent().addClass('parent');

//     alert("Hello! I am an alert box!!");

//     showMenu();

// });

(function ($) {
    $(document).ready(function(){
        $('.toggle').click(function() {

            // close menu item, set left arrow
            if ($(this).next('ul').hasClass('show')) {
                $(this).attr("aria-selected","false");
                $(this).attr("aria-expanded","false");
                $(this).next('ul').removeClass('show');
                //$(this).next('ul').slideUp(350);
                $(this).parent().parent().removeClass('open');
            }

            // open menu item
            else {
                // close other items
                $('#hfcrespmenu').children('.parent').children('.show').removeClass('show');
                $('#hfcrespmenu').children('.parent').children('.toggle').attr("aria-selected","false");
                $('#hfcrespmenu').children('.parent').children('.toggle').attr("aria-expanded","false");
                // open this item
                $(this).attr("aria-expanded","true");
                $(this).attr("aria-selected","true");
                //$(this).parent().parent().find('.parent ul').slideUp(350);
                $(this).next('ul').addClass('show');
                //$(this).next('ul').slideToggle(350);
                $(this).parent().parent().addClass('open');
            }


         }); // end toggle.click

            // open menu
            $('#hfcrespmenu-open').click(function() {
                    if ($('#hfcrespmenu').hasClass('show')) {
                        $('#hfcrespmenu').removeClass('show');
                        $(this).attr("aria-expanded","false");
                        $(this).attr("aria-selected","false");
                        $(this).removeClass('open');
                        $('#hfcrespmenu').children('.parent').children('.show').removeClass('show');
                        $('#hfcrespmenu').children('.parent').children('.toggle').attr("aria-selected","false");
                        $('#hfcrespmenu').children('.parent').children('.toggle').attr("aria-expanded","false");
                        $(".hfcrespmenu-button").html("Menu");
                    }
                    else {
                        $('#hfcrespmenu').addClass('show');
                        $(this).attr("aria-expanded","true");
                        $(this).attr("aria-selected","true");
                        $(this).addClass('open');
                        $(".hfcrespmenu-button").html("Close Menu");
                    }
            }); // end hfcrespmenu-open.click

            $('#hfcrespmenu').children().children().children().children().addClass('sub-item');

            $('.sub-item').focus(function() {
                $('#hfcrespmenu').children('.parent').children('.show').removeClass('show');
                $('#hfcrespmenu').children('.parent').children('.toggle').attr("aria-selected","false");
                $('#hfcrespmenu').children('.parent').children('.toggle').attr("aria-expanded","false");
                $('#hfcrespmenu').children('.parent').children('.toggle').next('ul').removeClass('show');
                $(this).parent().parent().addClass('show');
                $(this).parent('li').parent('ul').parent('.parent').children('.toggle').attr("aria-expanded","true");
                $(this).parent('li').parent('ul').parent('.parent').children('.toggle').attr("aria-selected","true");

                }); // end focus expand

    });
})(jQuery); // end showMenu function


// when one of the parent list items is toggled to reveal show go back up tree and toggle all of its siblings to be show
