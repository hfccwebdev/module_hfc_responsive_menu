<?php

/**
 * Callbacks for the Accordion module blocks.
 */
class ResponsiveMenuBlock {

  /**
   * Stores the menu name for this block.
   */
  protected $menu_name;

  /**
   * Instantiates a new object of this class.
   */
  public static function create() {
    return new static();
  }

  /**
   * Construct new object.
   */
  public function __construct() {
    $this->menu_name = variable_get('hfc_responsive_menu_menu_name', 'main-menu');
  }

  /**
   * Returns values for hook_block_info().
   */
  public static function info() {
    return [
      'info' => t('Responsive Menu'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return !empty(menu_get_menus()[$this->menu_name]) ? menu_get_menus()[$this->menu_name] : $this->menu_name;
  }

  /**
   * Returns form for hook_block_configure().
   */
  public function configure() {
    $form = [];
    $form['hfc_responsive_menu_menu_name'] = [
       '#type' => 'select',
       '#title' => t('Menu to display'),
       '#options' => menu_get_menus(),
       '#default_value' => $this->menu_name,
       '#description' => t('Select the menu for this block to display.'),
    ];
    return $form;
  }

  /**
   * Saves configuration for hook_block_save().
   */
  public function save($edit) {
    variable_set("hfc_responsive_menu_menu_name", $edit["hfc_responsive_menu_menu_name"]);
  }

  /**
   * Returns value for hook_block_view().
   */
  public function view() {
    $output = [];
    $this->build($output);

    if (!empty($output)) {
      return ['subject' => $this->label(), 'content' => $output];
    }
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {

    $items = menu_tree_page_data($this->menu_name, 1);

    $output = '<div id="hfcrespmenu-open"><button class="hfcrespmenu-button">Menu</button></div>';
    $output .= '<ul class="menu" id="hfcrespmenu">';

    foreach ($items as $item) {

      $options = [];
      if ($item['link']['in_active_trail']) {
        $options['attributes'] = ['class' => ['active-trail']];
      }

      if (!$item['link']['hidden']) {
        $output .= '<li>' . l($item['link']['link_title'], $item['link']['link_path'], $options) . '</li>';
      }
    }
    $output .= '</ul>';
    return $output;
  }
}
