<?php

/**
 * Contains the ResponsiveMenuRequirements class.
 */
class ResponsiveMenuRequirements {

  /**
   * Return content for hook_requirements().
   */
  public static function build($phase) {
    $requirements = [];
    // Ensure translations don't break during installation.
    $t = get_t();

    // Runtime phase shows information on the status report page.
    if ($phase == 'runtime') {

      // STOP TURNING THIS ON!!! WHO IS DOING THIS???
      if (module_exists('accordion')) {
        $requirements['hfc_responsive_menu']  = [
          'title' => $t('HFC Responsive Menu'),
          'value' => $t('Using HFC Responsive Menu and Accordion Menu modules together is not recommended!'),
          'severity' => REQUIREMENT_ERROR,
          'weight' => -99,
        ];
      }
    }
    return $requirements;
  }
}
